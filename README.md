# LayUI拓展组件：TreeSelect树形下拉选择器

#### 项目介绍
基于 layui 的树形下拉选择器。支持异步加载，提供点击回调函数，提供设置占位符、选中节点等方法。

#### 效果图

![效果图1](https://images.gitee.com/uploads/images/2018/0829/183243_660e69a2_1157021.png "image1.png")
![效果图2](https://images.gitee.com/uploads/images/2018/0829/183258_1d51d405_1157021.png "image2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0829/184111_d63c04d0_1157021.png "image3.png")

#### 使用示例

```
<input type="text" id="pidSelect" lay-filter="pidSelect" placeholder="选择父节点" class="layui-input">

<script>
    layui.use(['treeSelect'], function () {
        var treeSelect= layui.treeSelect;
            
            // 初始化下拉选择器
            treeSelect.render({
                // css选择器，推荐使用id
                elem: '#pidSelect',   
                // 请求地址
                data: '/system/menu/layui/tree', 
                // ajax请求方式：post/get
                type: 'post',  
                // 返回数据中主键的属性名称，默认值为id
                key: {
                    id: 'id',
                },
                // 节点点击回调函数
                click: function (d) {  
                    console.log(d);
                }
            });
            
            /* 
             *  手动设置占位符placeholder（不常用）
             *  第一个参数为lay-filter属性的值
             *  第二个参数为需要修改的提示内容
             */
            treeSelect.setTitle('pidSelect', '这里填写要改的内容');
            
            /* 
             *  选中节点（常用于更新时默认选中节点）
             *  第一个参数为lay-filter属性的值
             *  第二个参数为需要选中的节点的id
             */
            treeSelect.checkNode('pidSelect', 0);
    });
</script>

```

#### 数据格式参考
```
[
  
  {
    "children": [
      {
        "name": "留言列表",
        "icon": "&#xe62d;",
        "id": 5,
        "spread": false
      },
      {
        "name": "发表留言",
        "icon": "&#xe61f;",
        "id": 6,
        "spread": false
      }
    ],
    "name": "评论",
    "icon": "&#xe6af;",
    "id": 4,
    "spread": false
  },
  {
    "children": [
      {
        "children": [
          {
            "name": "添加用户",
            "icon": "",
            "id": 40,
            "spread": false
          },
          {
            "name": "编辑用户",
            "icon": "",
            "id": 41,
            "spread": false
          },
          {
            "name": "删除用户",
            "icon": "",
            "id": 42,
            "spread": false
          }
        ],
        "name": "用户列表",
        "icon": "",
        "id": 8,
        "spread": false
      },
      {
        "name": "角色列表",
        "icon": "",
        "id": 11,
        "spread": false
      },
      {
        "children": [
          {
            "name": "添加权限",
            "icon": "",
            "id": 34,
            "spread": false
          },
          {
            "name": "编辑权限",
            "icon": "",
            "id": 37,
            "spread": false
          },
          {
            "name": "删除权限",
            "icon": "",
            "id": 38,
            "spread": false
          }
        ],
        "name": "所有权限",
        "icon": "",
        "id": 13,
        "spread": false
      },
      {
        "name": "操作日志",
        "icon": "&#xe705;",
        "id": 15,
        "spread": false
      }
    ],
    "name": "权限管理",
    "icon": "",
    "id": 10,
    "spread": false
  }
]
```